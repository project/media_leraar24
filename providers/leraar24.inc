<?php

/**
 * @file
 *  This is an leraar24 provider include file for Embedded Media Video.
 *  Use this as a base for creating new provider files.
 *
 *  When using this, first make the following global replacements:
 *    * Replace LERAAR24 with the name of your provider in all caps.
 *    * Replace leraar24 with the name of your provider in all lower case.
 *    * Replace Example with the name (to be translated) of your provider in
 *        uppercase.
 *
 *  You then need to go through each function and modify according to the
 *  requirements of your provider's API.
 */

/**
 *  This is the main URL for your provider.
 */
define('EMVIDEO_LERAAR24_MAIN_URL', 'http://www.leraar24.nl/');

/**
 *  This is the URL to the API of your provider, if this exists.
 */
// define('EMVIDEO_LERAAR24_API_URL', 'http://www.leraar24.nl/api');

/**
 *  This defines the version of the content data array that we serialize
 *  in emvideo_leraar24_data(). If we change the expected keys of that array,
 *  we must increment this value, which will allow older content to be updated
 *  to the new version automatically.
 */
define('EMVIDEO_LERAAR24_DATA_VERSION', 1);

/**
 * hook emvideo_PROVIDER_info
 * This returns information relevant to a specific 3rd party video provider.
 *
 * @return
 *   A keyed array of strings requested by various admin and other forms.
 *    'provider' => The machine name of the provider. This must be the same as
 *      the base name of this filename, before the .inc extension.
 *    'name' => The translated name of the provider.
 *    'url' => The url to the main page for the provider.
 *    'settings_description' => A description of the provider that will be
 *      posted in the admin settings form.
 *    'supported_features' => An array of rows describing the state of certain
 *      supported features by the provider. These will be rendered in a table,
 *      with the columns being 'Feature', 'Supported', 'Notes'. In general,
 *      the 'Feature' column will give the name of the feature, 'Supported'
 *      will be Yes or No, and 'Notes' will give an optional description or
 *      caveats to the feature.
 */
function emvideo_leraar24_info() {
  $features = array(
    // array(t('Autoplay'), t('Yes'), ''),
    array(t('RSS Attachment'), t('Yes'), ''),
    array(t('Thumbnails'), t('Yes'), t('')),
    array(t('Full screen mode'), t('Yes'), t('You may customize the player to enable or disable full screen playback. Full screen mode is enabled by default.')),
  );
  return array(
    'provider' => 'leraar24',
    'name' => t('Leraar24'),
    'url' => EMVIDEO_LERAAR24_MAIN_URL,
    'settings_description' => t('These settings specifically affect videos displayed from !example. You can also read more about its !api.', array('!leraar24' => l(t('Leraar24.nl'), EMVIDEO_LERAAR24_MAIN_URL), '!api' => l(t("developer's API"), EMVIDEO_LERAAR24_API_URL))),
    'supported_features' => $features,
  );
}

/**
 *  hook emvideo_PROVIDER_settings
 *  This should return a subform to be added to the emvideo_settings() admin
 *  settings page.
 *
 *  Note that a form field set will already be provided at $form['leraar24'],
 *  so if you want specific provider settings within that field set, you should
 *  add the elements to that form array element.
 */
function emvideo_leraar24_settings() {
  // We'll add a field set of player options here. You may add other options
  // to this element, or remove the field set entirely if there are no
  // user-configurable options allowed by the leraar24 provider.
  $form['leraar24']['player_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Embedded video player options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  // This is an option to set the video to full screen. You should remove this
  // option if it is not provided by the leraar24 provider.
  $form['leraar24']['player_options']['emvideo_leraar24_full_screen'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow fullscreen'),
    '#default_value' => variable_get('emvideo_leraar24_full_screen', 1),
    '#description' => t('Allow users to view video using the entire computer screen.'),
  );

  return $form;
}

/**
 *  hook emvideo_PROVIDER_extract
 *
 *  This is called to extract the video code from a pasted URL or embed code.
 *
 *  We'll be passed a URL or the embed code from a video when an editor pastes
 *  that in the field's textfield. We'll need to either pass back an array of
 *  regex expressions to match, or do the matching ourselves and return the
 *  resulting video code.
 *
 *  @param $parse
 *    An optional string with the pasted URL or embed code.
 *  @return
 *    Either an array of regex expressions to be tested, or a string with the
 *    video code to be used. If the hook tests the code itself, it should
 *    return either the string of the video code (if matched), or an empty
 *    array. Otherwise, the calling function will handle testing the embed code
 *    against each regex string in the returned array.
 */
function emvideo_leraar24_extract($parse = '') {
  // Here we assume that a URL will be passed in the form of
  // http://www.leraar24.nl/video/text-video-title
  // or embed code in the form of <object value="http://www.leraar24.nl/embed...".

  // We'll simply return an array of regular expressions for Embedded Media
  // Field to handle for us.
  return array(
    // In this expression, we're looking first for text matching the expression
    // between the @ signs. The 'i' at the end means we don't care about the
    // case. Thus, if someone enters http://www.Example.nl, it will still
    // match. We escape periods as \., as otherwise they match any character.
    // The text in parentheses () will be returned as the provider video code,
    // if there's a match for the entire expression. In this particular case,
    // ([^?]+) means to match one more more characters (+) that are not a
    // question mark ([^\?]), which would denote a query in the URL.
    '@leraar24\.nl/video/([^\?]+)@i',
    '@leraar24\.nl:80/video/([^\?]+)@i',
    '@leraar24\.nl/video/([^"]+)=@i',
    '@leraar24\.nl:80/video/([^"]+)=@i',
  );
}

/**
 *  hook emvideo_PROVIDER_data
 *
 *  Provides an array to be serialised and made available with $item elsewhere.
 *
 *  This data can be used to store any extraneous information available
 *  specifically to the leraar24 provider.
 */

function emvideo_leraar24_data($field, $item) {
  $data = array();
  // Create some 'field' version control.
  $data['emvideo_leraar24_version'] = EMVIDEO_LERAAR24_DATA_VERSION;

  $video_id = emvideo_leraar24_extract($item['value']);

  // Be nice to make this an array for changing media:thumbnail?
  $data['thumbnail'] = 'http://www.leraar24.nl/api/video/' . $item['value'] . '/still.png';

  return $data;
}

/**
 *  hook emvideo_PROVIDER_rss
 *
 *  This attaches a file to an RSS feed.
 */
function emvideo_leraar24_rss($item, $teaser = NULL) {
  if ($item['value']) {
    $file['thumbnail']['filepath'] = $item['data']['thumbnail'];

    return $file;
  }
}

/**
 * hook emvideo_PROVIDER_embedded_link($video_code)
 * returns a link to view the video at the provider's site.
 *  @param $video_code
 *    The string containing the video to watch.
 *  @return
 *    A string containing the URL to view the video at the original provider's site.
 */
function emvideo_leraar24_embedded_link($item) {
  return 'http://www.leraar24.nl/video/'. $item;
}

/**
 * The embedded flash displaying the leraar24 video.
 */
function theme_emvideo_leraar24_flash($item, $width, $height, $autoplay) {
  $output = '';
  if ($item['embed']) {
    $autoplay = $autoplay ? 'true' : 'false';
    $fullscreen = variable_get('emvideo_leraar24_full_screen', 1) ? 'true' : 'false';
    $output .= '<object width="'. $width .'" height="'. $height .'">';
    $output .= '<param name="movie" value="http://www.leraar24.nl/leraar24-portlets/swf/player.swf">';
    $output .= '<param name="allowFullScreen" value="'. $fullscreen .'" />';
    $output .= '<param name="wmode" value="opaque" />';
    $output .= '<param name="allowscriptaccess" value="always" />';
    $output .= '<param name="FlashVars" value="file=http://www.leraar24.nl/leraar24-portlets/playlist/smil.xml?id='. $item['value'] .'" />';
    $output .= '<embed flashvars="file=http://www.leraar24.nl/leraar24-portlets/playlist/smil.xml?id='. $item['value'] .'" videolink="http://www.leraar24.nl/video/'. $item['value'] .'" src="http://www.leraar24.nl/leraar24-portlets/swf/player.swf" type="application/x-shockwave-flash" allowFullScreen="'. $fullscreen .'"  width="'. $width .'" height="'. $height .'"></embed>';
    $output .= '</object>';
  }
  return $output;
}

/**
 * hook emvideo_PROVIDER_thumbnail
 * Returns the external url for a thumbnail of a specific video.
 *  @param $field
 *    The field of the requesting node.
 *  @param $item
 *    The actual content of the field from the requesting node.
 *  @return
 *    A URL pointing to the thumbnail.
 */
function emvideo_leraar24_thumbnail($field, $item, $formatter, $node, $width, $height) {
  return 'http://www.leraar24.nl/api/video/' . $item['value'] . '/still.png';
}

/**
 *  hook emvideo_PROVIDER_video
 *  This actually displays the full/normal-sized video we want, usually on the
 *  default page view.
 *  @param $embed
 *    The video code for the video to embed.
 *  @param $width
 *    The width to display the video.
 *  @param $height
 *    The height to display the video.
 *  @param $field
 *    The field info from the requesting node.
 *  @param $item
 *    The actual content from the field.
 *  @return
 *    The html of the embedded video.
 */
function emvideo_leraar24_video($embed, $width, $height, $field, $item, $node, $autoplay) {
  $output = theme('emvideo_leraar24_flash', $item, $width, $height, $autoplay);
  return $output;
}

/**
 *  hook emvideo_PROVIDER_video
 *
 *  This actually displays the preview-sized video we want, commonly for the
 *  teaser.
 *  @param $embed
 *    The video code for the video to embed.
 *  @param $width
 *    The width to display the video.
 *  @param $height
 *    The height to display the video.
 *  @param $field
 *    The field info from the requesting node.
 *  @param $item
 *    The actual content from the field.
 *  @return
 *    The html of the embedded video.
 */
function emvideo_leraar24_preview($embed, $width, $height, $field, $item, $node, $autoplay) {
  $output = theme('emvideo_leraar24_flash', $item, $width, $height, $autoplay);
  return $output;
}

/**
 *  Implementation of hook_emfield_subtheme.
 *  This returns any theme functions defined by this provider.
 */
function emvideo_leraar24_emfield_subtheme() {
  $themes = array(
    'emvideo_leraar24_flash'  => array(
      'arguments' => array('item' => NULL, 'width' => NULL, 'height' => NULL, 'autoplay' => NULL),
      'file' => 'providers/leraar24.inc',
      // If you don't provide a 'path' value, then it will default to
      // the emvideo.module path. Obviously, replace 'emleraar24' with
      // the actual name of your custom module.
      'path' => drupal_get_path('module', 'media_leraar24'),
    )
  );
  return $themes;
}
